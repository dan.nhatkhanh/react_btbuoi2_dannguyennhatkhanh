import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
import styles from "./glasses.module.css";
import model from "./img/model.jpg";
import RenderGlassOnModel from "./RenderGlassOnModel";
import RenderImage from "./RenderImage";

export default class BodyComponent extends Component {
  state = {
    glassArr: dataGlasses,
    avatarItem: {},
  };

  handleShowAvatar = (id) => {
    let glass = this.state.glassArr.find((item) => {
      return item.id === id;
    });
    console.log("glass: ", glass);

    this.setState({
      avatarItem: glass,
    });
  };

  render() {
    return (
      <div className={styles.glass_body}>
        <div className={styles.glass_model}>
          <img className={styles.model_img} src={model} alt="model" />

          <RenderGlassOnModel avatarItem={this.state?.avatarItem} />
        </div>

        <div class={styles.container}>
          <div className={styles.glasses_box}>
            <div className={styles.glasses_list}>
              {this.state.glassArr.map((item, index) => {
                return (
                  <RenderImage
                    data={item}
                    key={index.toString() + item.id}
                    handleShowAvatar={this.handleShowAvatar}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
