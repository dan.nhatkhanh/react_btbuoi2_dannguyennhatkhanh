import React, { Component } from "react";
import styles from "./glasses.module.css";

export default class HeaderComponent extends Component {
  render() {
    return (
      <div className={styles.header}>
        <h2>TRY GLASSES APP ONLINE</h2>
      </div>
    );
  }
}
