import React, { Component } from "react";
import styles from "./glasses.module.css";

export default class RenderImage extends Component {
  render() {
    return (
      <>
        <button
          onClick={() => {
            this.props.handleShowAvatar(this.props.data.id);
          }}
          className={styles.glass_btn}
        >
          <img
            className={styles.glasses_img}
            src={require(`${this.props.data.src}`)}
            alt="img"
          />
        </button>
      </>
    );
  }
}
