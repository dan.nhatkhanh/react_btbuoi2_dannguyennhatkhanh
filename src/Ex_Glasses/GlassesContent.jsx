import React, { Component } from "react";
import BodyComponent from "./BodyComponent";
import styles from "./glasses.module.css";
import HeaderComponen from "./HeaderComponent";

export default class GlassesContent extends Component {
  render() {
    return (
      <div className={styles.bg}>
        <HeaderComponen />

        <BodyComponent />
      </div>
    );
  }
}
