import React, { Component } from "react";
import styles from "./glasses.module.css";

export default class RenderGlassOnModel extends Component {
  render() {
    return (
      Object.keys(this.props.avatarItem).length > 0 && (
        <div>
          <div className={styles.avatar}>
            <img
              id={styles.avatar_img}
              src={require(`${this.props.avatarItem?.url}`)}
              alt="avatar"
            />
          </div>
          <div className={styles.glass_detail}>
            <h3>{this.props.avatarItem?.name}</h3>
            <h4>${this.props.avatarItem?.price}</h4>
            <p>{this.props.avatarItem?.desc}</p>
          </div>
        </div>
      )
    );
  }
}
