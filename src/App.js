import logo from "./logo.svg";
import "./App.css";
import GlassesContent from "./Ex_Glasses/GlassesContent";

function App() {
  return (
    <div className="App">
      <GlassesContent />
    </div>
  );
}

export default App;
